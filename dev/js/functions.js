$(document).ready(function(){
	//Add @keyfarme to head
	styleCSS();
	//Loading
	loader(3000);

	TextMV($('.js-text'));
	setTimeout(function(){
		$('.c-logo').addClass('is-show');
		$(".c-slider__wrap").slide(4000);
	}, 3000);

	//Click logo
	addLoader_logo($('.c-logo'),2000);
});

function loader(time){
	$('body').append('<div class="c-load"></div>');
	var timeAnimation = (time/1000)/2;
	var timeOut = (time + 500);
	var timeRemove = (time + 700);
	var wItem = 20;
	var nItem = 100/wItem;
	var $area = $('.c-load');
	var strArea ='';
	var styleItem = 'background: #000; display: block; width: '+wItem+'vw; height: 100vh; position: absolute; top: 0px; z-index: 1;transition: 0.5s;';
	for( var i = 0; i < nItem; i++){
		var leftItem = wItem*i;
		strArea+='<span style="'+styleItem+'left: '+leftItem+'vw"></span>';	
	}
	$area.append(strArea+'<div class="c-load__line"><span></span></div>').css({
		width: "100%",
		height: "100%",
		position: "fixed",
		top: "0",
		left: "0",
		zIndex: "8"
	});
	setTimeout(function(){
		$('.c-load span').css({
			width: "0",
		});
	}, time);
	setTimeout(function(){
		$('.c-load').fadeOut(100);
	}, timeOut);
	setTimeout(function(){
		$('.c-load').remove();
	}, timeRemove);
	$('.c-load__line').css({
		width: "70%",
		height: "2px",
		background: "rgba(255, 255, 255, 0.4)",
		position: "absolute",
		left: "15%",
		bottom: "20vh",
		zIndex: "1",
		overflow: "hidden",
	});
	$('.c-load__line span').css({
		display: "block",
		width: "100%",
		height: "100%",
		background: "#FFF",
		position: "absolute",
		top: "0",
		left: "-100%",
		animation: "loader "+timeAnimation+"s cubic-bezier(0, 0, 1, 1) infinite",
	});
}

function addLoader_logo(element,time){	
	$(element).on('click', function() {
		$('body').append('<div class="c-loadLogo" style="width: 100%;height: 100%;position: fixed; top: 0;left: 0;z-index: 12;"><div class="c-loadLogo__wrap"></div></div>');
		var timeAnimation = (time/1000)/2;
		var timeLogo = (time/1000)/3;
		var timeOut = (time - 300);
		var wItem = 25;
		var nItem = 100/wItem;
		var loadLogo = $('.c-loadLogo__wrap');
		var strloadLogo ='';
		var styleItem = 'background: #000; display: block; width: '+wItem+'vw; height: 100vh; position: absolute; top: 0px; z-index: 1; transform: scale(0,2) rotate(0deg);transition: '+timeLogo+'s cubic-bezier(.22,1,.36,1) 0s;';
		for( var i = 0; i < nItem; i++){
			var leftItem = wItem*i;
			strloadLogo+='<span class="c-loadLogo__item" style="'+styleItem+'left: '+leftItem+'vw"></span>';	
		}
		loadLogo.append('<span class="c-loadLogo__item" style="'+styleItem+'left:-'+wItem+'vw;"></span>'+strloadLogo+'<span class="c-loadLogo__item" style="'+styleItem+'left:100vw;"></span>');
		setTimeout(function(){
			$('.c-loadLogo .c-loadLogo__item').css({
				transform: "scaleY(2) rotate(-25deg)"
			});
			$('.c-loadLogo').append('<div class="c-loadLogo__line"><span></span></div>');
			$('.c-loadLogo__line').css({
				width: "70%",
				height: "2px",
				background: "rgba(255, 255, 255, 0.4)",
				position: "absolute",
				left: "15%",
				bottom: "50%",
				zIndex: "1",
				overflow: "hidden",
			});
			$('.c-loadLogo__line span').css({
				display: "block",
				width: "100%",
				height: "100%",
				background: "#FFF",
				position: "absolute",
				top: "0",
				left: "-100%",
				animation: "loader "+timeAnimation+"s cubic-bezier(0, 0, 1, 1) infinite",
			});
		}, 200);
		setTimeout(function(){
			$('.c-loadLogo .c-loadLogo__item').css({
				transform: "scale(0,1.4) rotate(0rad)",
			});
			$('.c-loadLogo__line').remove();
		}, timeOut);
		setTimeout(function(){
			$('.c-loadLogo').remove();
		}, time);
	});
}

(function ( $ ) {
	$.fn.slide = function(time){
		var wrap = 'js-slider';
		var wrap_item = 'js-slider__item';
		var img = 'js-slider__img';
		var control = 'js-slider__control';
		var number_Current = 'js-slider__numberCurrent';
		var number_Next = 'js-slider__numberNext';
		var line = 'js-slider__line';
		var dotted = 'js-slider__dotted';

		var active = 'is-active';

		//Add necessary className and element
		this.addClass(wrap);
		this.children().addClass(wrap_item);
		this.children().find('img').addClass(img);
		this.parent().append('<div class="'+control+'"><div class="'+number_Current+'"></div><div class="'+line+'"><span></span></div><div class="'+number_Next+'"></div><div class="'+dotted+'"></div></div>');
		
		//set variable
		var item = $('.'+wrap_item);
		var numberSlide = item.length;

		var $numberCurrent = $('.'+number_Current);
		var $numberNext = $('.'+number_Next);
		$numberCurrent.html('01');
		$numberNext.html('02');
		
		//enable the first item
        item.eq(0).addClass(active);

		//Transition slide
		setInterval(function(){
			var nextSlide, nextDot, sttSlide, sttwait, imgItem;
			var currentSlide= $('.'+wrap+' .'+wrap_item+'.'+active);
			var currentDot  = $('.'+dotted+' .item.'+active);

			var itemdot = $('.'+dotted+' .item').delegate();

			//set position to  next slider
			if(currentSlide.is(":last-child")){
				nextSlide = item.eq(0);
				nextDot = itemdot.eq(0);
				sttSlide = 1;
                sttwait = 2;
			}else{
				nextSlide = currentSlide.next();
				nextDot = currentDot.next();
				sttSlide= currentSlide.next().index()+1;
				sttwait = sttSlide+1;
			}			

			//Change status active
			currentSlide.removeClass(active);			
			nextSlide.addClass(active);

			currentDot.removeClass(active);
			nextDot.addClass(active);

			//add dot number
			if(sttwait>numberSlide){
                sttwait = 1;
            }

            //Get IMG item
            if(item.hasClass(active)){
            	var srcIMG = $('.'+wrap_item+'.'+active).find('.'+img).attr('src');
            	$('.'+wrap_item+' span').css({backgroundImage: 'url("'+srcIMG+'")'});
            }
            
            $numberCurrent.html('0'+sttSlide+'');
            $('.'+number_Current+' span').addClass(active);        
            $numberNext.html('0'+sttwait+'');

		}, time);
		createItem(item);
		wideget(item);
		createCSS(item);	
		
		return this;

		function createItem() {
			//var images = $('.'+img).eq(index).attr('src');
			var timeAnimation = (time/1000);
			var hItem = 3;
			var $number = 100/hItem;
			var $area = item;
			var strArea ='';
			var styleItem = 'background-repeat: no-repeat; background-size: auto 100vh; display: block; width: 100%; height:'+hItem+'vh; position: absolute; left: -100%; z-index: 1; animation: image '+timeAnimation+'s ease-in-out infinite;';
			for( var $i = 0; $i < $number; $i++){
					var posTop = hItem*$i;
					var aniDelay = 0.015*($i/2);
					var aniDelayLast = 0.0135;
					var last = $number/2;
				if($i % 2 == 0){
					strArea+='<span style="background-position: center -'+posTop+'vh;'+styleItem+'top:'+posTop+'vh;animation-delay: '+aniDelay+'s;"></span>';
				} else{
					strArea+='<span style="background-position: center -'+posTop+'vh;'+styleItem+'top:'+posTop+'vh; animation-delay:0.135s;"></span>';
				}		
			}
			$area.append(strArea);		
		}

		function createCSS() {
			var timeAnimation = (time/1000);
			$('.'+wrap).css({
				position: "relative",
				width: "100%",
				height: "100%",
				zIndex: "1",
			});
			$('.'+wrap_item).css({
				width: "100%",
				height: "100%",
				position: "absolute",
				top: "0",
				left: "0",
				zIndex: "1",
			});
			$('.'+wrap_item+' span').css({backgroundImage: 'url("/assets/img/common/mv01.png")'});
			$('.'+img).css({
				opacity: "0",
				width: "0"
			});
			$('.'+control).css({
				display: "block",
				position: "absolute",
				right: "2%",
				top: "0",
				paddingTop: "5%",
				width: "20px",
				height: "100%",
				zIndex: "2",
			});
			$('.'+line).css({
				height: "25vh",
				width: "2px",
				background: "rgba(255, 255, 255, 0.4)",
				margin: "20px auto",
				position: "relative",
				overflow: "hidden",
			});
			$('.'+line+' span').css({
				height: "100%",
				width: "100%",
				background: "#FFF",
				position: "absolute",
				top: "-100%",
				left: "0",
				animation: "line "+timeAnimation+"s cubic-bezier(0, 0, 1, 1) infinite",
			});
			$numberCurrent.css({
				width: "100%",
				fontSize: "1.2rem",
				color: "#FFF",
				fontWeight: "bold",
				textAlign: "center",				
				animation: "number "+timeAnimation+"s ease-in-out infinite",
			});
			$numberNext.css({
				"width": "100%",
				"font-size": "1.2rem",
				"color": "#FFF",
				"font-weight": "bold",
				"text-align": "center",				
				"animation": "number "+timeAnimation+"s ease-in-out infinite",
			});
		}

		function wideget(ele) {
			var size = ele.length;
			var dot = $('.'+dotted);
			var strDotted ='';
			for(var i=1; i<=size; i++){
		            var spanClass = i==1?' '+active:'';
				strDotted+='<span class="item'+spanClass+'"></span>';
		    }
		    dot.append(strDotted);
		}

		
	}
}( jQuery ));

function TextMV(element) {
    $(element).contents().each(function() {
        if(this.nodeType === 1) {
            TextMV(this);
        }
        else if(this.nodeType === 3) {
            $(this).replaceWith($.map(this.nodeValue.split(''), function(c) {
            	return '<span><b>' + c + '</b></span>';
            }).join(''));
        }
    });
} 

function styleCSS() {
	var ruleCSS = "@keyframes loader { 0%{ left: -100%; } 100%{ left: 100%; }} @keyframes line { 0%{ top: -100%; } 100%{ top: 0%; }} @keyframes number {0%{transform: translateX(50%);opacity:0;}8%,100%{transform: translateX(0);opacity:1;}} @keyframes image {0%{left: 100%;opacity:0}20%,85%{left: 0%;opacity:1}100%{left: -100%;opacity:0}}";
	$('<style>'+ruleCSS+'</style>').appendTo('head');
}
